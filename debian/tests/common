# common code used by the DEP8 tests

check_status() {
    local status_code=$1
    local msg="${2:-task}"

    if [ "$status_code" -ne "0" ]; then
        echo "ERROR: $msg failed, exit status was $status_code"
        exit "$status_code"
    else
        echo "OK: $msg succeeded"
    fi
}

populate_directory() {
    local target="$1"
    local tempfile

    test -d "${target}" || mkdir -p -m 0755 "${target}"

    for n in $(seq 1 10); do
        tempfile=$(mktemp "$target/tmp.XXXXXX")
        # the date command makes each tempfile different
        # the seq command just produces a lot of output
        # using a "here" document as to not pollute the set -x
        # output with large command lines
        cat > "$tempfile" <<EOF
$(date +%s%N)
$(seq 1 81920)
EOF
    chmod 0644 "$tempfile"
    done
}

add_samba_share() {
	local sharename="${1}"
    local sharepath="${2}"
    local guestok="${3}" # "yes" or "no"
    if ! testparm -s 2>&1 | grep -qE "^\[${sharename}\]"; then
        echo "Adding [${sharename}] share"
        cat >> /etc/samba/smb.conf <<EOF
[${sharename}]
  path = "${sharepath}"
  read only = no
  guest ok = ${guestok}
EOF
        service smbd reload
    else
        echo "No need to add [${sharename}] share, continuing."
    fi
}

add_localhost_backuppc_config() {
    local sharename="${1}"
    local smbuser="${2:-guest}"
    local password="${3:-nothing}"
    cat > /etc/backuppc/localhost.pl <<EOF
\$Conf{XferMethod} = 'smb';
\$Conf{XferLogLevel} = 1;
\$Conf{ClientCharset} = '';
\$Conf{ClientCharsetLegacy} = 'iso-8859-1';
\$Conf{SmbShareName} = '${sharename}';
\$Conf{SmbShareUserName} = '${smbuser}';
\$Conf{SmbSharePasswd} = '${password}';
\$Conf{PingPath} = '/bin/ping';
\$Conf{Ping6Path} = '/bin/ping6';
EOF
}

add_user() {
    local username="$1"
    local password="$2"
    echo "Creating a local and samba user called ${username}"
    useradd -m "${username}"
    echo "Setting samba password for the ${username} user"
    (echo "${password}"; echo "${password}") | smbpasswd -s -a ${username}
}

